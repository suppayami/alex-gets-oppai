-- common modules
JSON = require 'common/json'
require 'common/object'
require 'common/sort'
require 'common/round'
-- core engine
require 'config'
require 'core/run'
-- classes & objects
require 'class/cache'
require 'class/datamanager'
require 'class/layermanager'
require 'class/sprite'
require 'class/spritebox'
require 'class/spriteground'
require 'class/spritewall'
require 'class/spriteplayer'
require 'class/spriteback'
require 'class/spriteoppai'
require 'class/quadtree'
require 'class/mapmanager'
require 'class/block'
require 'class/box'
require 'class/playerblock'
require 'class/oppaiblock'

function love.load()
    sysFont = love.graphics.newFont("fonts/OpenSans-Semibold.ttf", 26)
    love.graphics.setFont(sysFont)
    love.createDatabase()
    love.createTesting()
end

function love.draw()
    local score = math.round(MapManager.objects[4].maxY * 10) + MapManager.bonusPoints
    LayerManager:drawAll()
    love.graphics.setColor(0, 0, 0, 128)
    love.graphics.printf("Score: ", 16, 0, 640, "left")
    love.graphics.printf("Score: ", 14, 0, 640, "left")
    love.graphics.printf("Score: ", 15, 1, 640, "left")
    love.graphics.printf("Score: ", 15, -1, 640, "left")
    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.printf("Score: ", 15, 0, 640, "left")
    local img = Cache:loadImage("Numbers.png")
    --img:setFilter("linear", "linear")
    if score == 0 then
        love.graphics.draw(Cache:loadImage("Numbers.png"), love.graphics.newQuad(9 * 16, 0, 16, 16, 160, 16), 96, 8, 0, 1.5, 1.5)
    else
        local n = score
        local x = math.floor(math.log10(n))
        while n > 0 do
            local i = n % 10
            if i == 0 then i = 9 else i = i - 1 end
            love.graphics.draw(Cache:loadImage("Numbers.png"), love.graphics.newQuad(i * 16, 0, 16, 16, 160, 16), 96 + x * 16, 8, 0, 1.5, 1.5)
            n = math.floor(n / 10)
            x = x - 1
        end
    end
end

function love.update(dt)
    MapManager:update()
    LayerManager:updateAll()
end

function love.createTesting()
    DataManager:setup()
    MapManager:setup()
end

function love.createDatabase()
    do
        local file = io.open("others/database/general.json", "w")
        local data = {}
        data.scrollSpeed = 0.05
        data.scrollAccel = 0.000005
        file:write(JSON:encode(data))
        file:close()
    end

    do
        local file = io.open("others/database/players.json", "w")
        local data = {}
        data[1] = {}
        data[1].name = "Alex"
        data[1].jumpHeight = 68
        data[1].size = {34, 34}
        data[1].block = {idle={16,25}, wall={18,28}}
        data[1].moveSpeed = 4
        data[1].wallDistance = 34
        file:write(JSON:encode(data))
        file:close()
    end

    do
        local file = io.open("others/database/boxes.json", "w")
        local data = {}
        data[1] = {}
        data[1].image = "Block_1.png"
        data[1].fallSpeed = 4
        data[1].fallAccel = 0.1
        data[1].size = {28, 28}
        data[2] = {}
        data[2].image = "Block_2.png"
        data[2].fallSpeed = 4
        data[2].fallAccel = 0.1
        data[2].size = {56, 84}
        data[3] = {}
        data[3].image = "Block_3.png"
        data[3].fallSpeed = 4
        data[3].fallAccel = 0.1
        data[3].size = {28, 84}
        data[4] = {}
        data[4].image = "Block_4.png"
        data[4].fallSpeed = 4
        data[4].fallAccel = 0.1
        data[4].size = {84, 28}
        data[5] = {}
        data[5].image = "Block_5.png"
        data[5].fallSpeed = 4
        data[5].fallAccel = 0.1
        data[5].size = {56, 56}
        data[6] = {}
        data[6].image = "Block_6.png"
        data[6].fallSpeed = 4
        data[6].fallAccel = 0.1
        data[6].size = {56, 56}
        data[7] = {}
        data[7].image = "Block_7.png"
        data[7].fallSpeed = 4
        data[7].fallAccel = 0.1
        data[7].size = {28, 28}
        file:write(JSON:encode(data))
        file:close()
    end
end